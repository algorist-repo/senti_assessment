/*
The first link should detail a class or module which performs the key functionality of accepting a data stream from our device (assume device authentication has already occurred and the data stream will have been accompanied by a token demonstrating this authentication; similarly, assume that the data stream has already been unencrypted). This code should enable a remote client to pass a stream of data to the server.
*/

/* A basic implementation of stream usage with additional negotiation messaging
 * to support extended features:
 * - multi stream
 * - abort stream
 * - resume stream
 */

#include "message.hh"

using namespace std;

class ServerClass {

private:
    int sock;

    void init_socket(int portno,  struct sockaddr_in &server_addr)
    {
        int option_val = 1;
        sock = socket(AF_INET, SOCK_STREAM, 0);
        if (sock < 0)
        {
            cout << "Socket error" << endl;
            return;
        }
        setsockopt(sock, SOL_SOCKET, SO_REUSEADDR,
                   (const void *)&option_val , sizeof(int));

        bzero((char *) &server_addr, sizeof(server_addr));

        server_addr.sin_family = AF_INET;
        server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
        server_addr.sin_port = htons((unsigned short)portno);
    }

    void create_ack_msg (ack_t &ack)
    {
        msg_hdr_t header;
        header.msg_type = ACK;
        header.msg_len = sizeof(ack_t) - sizeof(msg_hdr_t);
        ack.header = header;
    }

    void create_nack_msg (nack_t &nack)
    {
        msg_hdr_t header;
        header.msg_type = NACK;
        header.msg_len = sizeof(nack_t) - sizeof(msg_hdr_t);
        nack.header = header;
    }

    void perform_action(int client_fd, char *buf)
    {
        msg_hdr_t *msg_hdr = (msg_hdr_t*)buf;
        s_transfer_t s_trans;
        e_transfer_t e_trans;
        ack_t ack;
        nack_t nack;
        packet_t pack;
        int n;
        char filename[32];
        static char file_path[64];
        FILE *fptr;

        switch(msg_hdr->msg_type) {

        case START_TRANS:
        {
            // create a file
            n = read(client_fd, &filename, msg_hdr->msg_len);
            cout << "created file." << filename  << endl;
            sprintf(file_path, "%s/%s", SND_DIR, filename);
            cout << file_path << endl;
            if((fptr= fopen(file_path,"w")) == NULL)
            {
                // send NACK
                create_nack_msg(nack);
                n = write(client_fd, &nack, sizeof(nack_t)); //nack
                cout << "[start_trans]:nack." << n  << endl;
            }
            else
            {
                // send ACK
                create_ack_msg(ack);
                n = write(client_fd, &ack, sizeof(ack_t)); //ack
                cout << "[start_trans]:ack." << n  << endl;
            }
            fclose(fptr);
            break;
        }
        case STREAM_DATA:
        {
            n = read(client_fd, pack.data, msg_hdr->msg_len);
            // write file content
            cout << "[stream data]:read." << n  << endl;
            if((fptr = fopen(file_path,"a+")) == NULL) {
                create_nack_msg(nack);
                write(client_fd, &nack, sizeof(ack_t)); //ack
                cout << "[stream data]:ack" << endl;
                break;
            }

            fwrite(pack.data, sizeof(PKT_SIZE), 1, fptr);
            fclose(fptr);
            // send ACK
            create_ack_msg(ack);
            write(client_fd, &ack, sizeof(ack_t)); //ack
            cout << "[stream data]:ack" << endl;
            break;
        }
        case END_TRANS:

            n = read(client_fd, &e_trans, msg_hdr->msg_len);
            // send ACK
            create_ack_msg(ack);
            write(client_fd, &ack, sizeof(ack_t)); //ack
            cout << "[end_tranc]:ack" << endl;
            break;
        }
    }
public:
    void start()
    {
        bool shutdown = false;
        int client_len;
        struct sockaddr_in server_addr;
        struct sockaddr_in client_addr;
        int client_fd = 0;
        char buf[sizeof(msg_hdr_t)];
        int n;
        int portno = 8888;
        fd_set active_fd_set, read_fd_set;
        int i;
        int max_sd;


        init_socket(portno, server_addr);

        if (bind(sock, (struct sockaddr *) &server_addr,
                 sizeof(server_addr)) < 0)
        {
            cout << "bind error " << endl;
            return;
        }

        if (listen(sock, MAX_CLIENT_QUE) < 0)
        {
            cout << "listen error " << endl;
            return;
        }
        FD_ZERO (&active_fd_set);

        FD_SET (sock, &active_fd_set);

        client_len = sizeof(client_addr);

        max_sd = sock;

        while (false == shutdown)
        {
            read_fd_set = active_fd_set;
            if (select (max_sd + 1, &read_fd_set, NULL, NULL, NULL) < 0)
            {
                cout << "select error " << endl;
                return;
            }
            if (FD_ISSET (sock, &read_fd_set))
            {
                client_fd = accept(sock, (struct sockaddr *) &client_addr, (socklen_t*)&client_len);
                if (client_fd < 0)
                {
                     cout << "accept error " << endl;
                     return;
                }
                FD_SET (client_fd, &active_fd_set);
                max_sd = client_fd;
            }
            if (FD_ISSET(client_fd , &read_fd_set))
            {
                bzero(buf, sizeof(msg_hdr_t));
                n = read(client_fd, buf, sizeof(msg_hdr_t));
                if (n > 0)
                    cout << "read:" << n << endl;
                // route actions
                perform_action(client_fd, buf);
            }
        }

        close(client_fd);
    }
};

int main () {
    ServerClass server;
    server.start();
}


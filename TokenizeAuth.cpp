/*
 * The second link should detail a different class or module, which performs some other key logic (for the system requirements) of your choosing. For example, this code could accomplish tokenised authentication of users, decryption of the data stream, storage of the data, error logging, or another function you consider to be critical.
 */

/* Basic Token Generator in C++, this can used with
 * any transport layer protocol, HTTP or bare
 * minimum sockets.
 * C11 std library usage
 * Compile command:  g++ TokenizeAuth.cpp -o TokenizeAuth -std=c++11
 */
#include <iostream>
#include <functional>
#include <string>
#include <map>
#define TOKEN_EXPIRE 15 // seconds

using namespace std;

class Token {

private:
    std::map<std::size_t, unsigned long> tokens;

public:
    bool verify (std::size_t token)
    {
        std::map<size_t,unsigned long>::iterator it;
        it = tokens.find(token);
        if (it != tokens.end())
        {
            if (it->second >= (unsigned long)time(NULL))
            {
                it->second =  TOKEN_EXPIRE + (unsigned long)time(NULL); // update expiry
                return true;
            }
            else
            {
                return false;
            }
        }
        else
            return false;
    }

    std::size_t generate (std::string id, std::string secret) {

        std::hash<std::string> hash_fn;
        std::size_t str_hash = hash_fn(id + secret);
        tokens.insert(pair<std::size_t, unsigned long>(str_hash, TOKEN_EXPIRE + (unsigned long)time(NULL)));
        return str_hash;
    }

    bool expire (std::size_t token)
    {
        std::map<size_t,unsigned long>::iterator it;
        it = tokens.find(token);
        if (it != tokens.end())
        {
            tokens.erase (it);
            return true;
        }
        return false;
    }
};


int main ()
{
    size_t token_id;

    Token token;
    token_id = token.generate("100", "secret");

    cout << "generate: "<< token_id << endl;
    cout << "verify: " << token.verify(token_id) << endl;
    cout << "expire: " << token.expire(token_id) << endl;
    cout << "verify: " << token.verify(token_id) << endl;

    return 0;
}
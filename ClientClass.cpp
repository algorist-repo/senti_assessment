/*
The first link should detail a class or module which performs the key functionality of accepting a data stream from our device (assume device authentication has already occurred and the data stream will have been accompanied by a token demonstrating this authentication; similarly, assume that the data stream has already been unencrypted). This code should enable a remote client to pass a stream of data to the server.
*/

#include "message.hh"

class ClientClass {

private:
    int sock;
    struct timeval tv;

    void init_socket(char *hostname, int portno,  struct sockaddr_in &server_addr)
    {
        int option_val = 1;
        struct hostent *server;

        sock = socket(AF_INET, SOCK_STREAM, 0);
        if (sock < 0)
        {
            cout << "socket error" << endl;
            return;
        }
        server = gethostbyname(hostname);
        if (server == NULL)
        {
            cout << "hostname error." << endl;
        }
        tv.tv_sec = 1;
        tv.tv_usec = 0;
        setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);
        bzero((char *) &server_addr, sizeof(server_addr));
        server_addr.sin_family = AF_INET;
        bcopy((char *)server->h_addr,
              (char *)&server_addr.sin_addr.s_addr, server->h_length);
        server_addr.sin_port = htons((unsigned short)portno);
    }

    int perform_action(int client_fd, char *buf)
    {
        msg_hdr_t *msg_hdr = (msg_hdr_t*)buf;
        ack_t ack;
        nack_t nack;
        int n;

        switch(msg_hdr->msg_type) {
        case ACK:
            cout << "ACK recieved." << endl;
            return 0;
        case NACK:
            cout << "NACK recieved." << endl;
            return -1;
        }
        return -1;
    }

    void create_tranfer_msg (s_transfer_t &transfer, const char *filename)
    {
        msg_hdr_t header;
        header.msg_type = START_TRANS;
        header.msg_len = sizeof(s_transfer_t) - sizeof(msg_hdr_t);
        transfer.header = header;
        memcpy(transfer.filename, filename, 32);

    }

    void create_end_msg (e_transfer_t &end)
    {
        msg_hdr_t header;
        header.msg_type = END_TRANS;
        header.msg_len = 0;
    }

    void create_stream_msg (packet_t &packet, const unsigned char *data)
    {
        msg_hdr_t header;
        header.msg_type = STREAM_DATA;
        header.msg_len = sizeof(packet_t) - sizeof(msg_hdr_t);
        packet.header = header;
        memcpy(packet.data, data, PKT_SIZE);
    }

public:

    void start_transfer(char *filename)
    {
        s_transfer_t transfer;
        create_tranfer_msg(transfer, filename);
        write(sock, &transfer, sizeof(s_transfer_t));
    }

    void end_tranfer()
    {
        e_transfer_t end;
        create_end_msg(end);
        write(sock, &end, sizeof(e_transfer_t));
    }

    void send_data(const unsigned char *data) {
        packet_t packet;
        create_stream_msg(packet, data);
        write(sock, &packet, sizeof(packet_t));
    }

    int wait_commmand() {
        int n;
        char buf[sizeof(msg_hdr_t)];

        bzero(buf, sizeof(msg_hdr_t));
        n = read(sock, buf, sizeof(msg_hdr_t));
        if (n > 0)
        {
            return perform_action(sock, buf);

        }
        else
        {
            return 0;

        }
    }

    void server_close() {
        close(sock);
    }

    void server_connect()
    {
        bool shutdown = false;
        int client_len;
        struct sockaddr_in server_addr;
        char buf[sizeof(msg_hdr_t)];
        int n;
        int portno = 8888;
        char hostname[32] = "127.0.0.1";


        init_socket(hostname, portno, server_addr);

        if (connect(sock, (sockaddr *)&server_addr, sizeof(server_addr)) < 0)
        {
            cout <<  "connection error." << endl;
        }
    }


};


int main () {

    char filename[32] = "sound.wav";

    unsigned char random_data[FILE_SIZE];

    unsigned char data[PKT_SIZE];

    int data_count = 0;



    for (int i = 0; i < PKT_SIZE; i++)
    {
        data[i] = 1;
    }

    ClientClass client;

    client.server_connect();

    client.start_transfer(filename);

    if (client.wait_commmand() == 0)
        cout << "Allowed transfer" << endl;

    // send stream

    client.send_data(data);

    client.wait_commmand();

    client.send_data(data);

    client.wait_commmand();

    // end transfer

    client.end_tranfer();

    cout <<  client.wait_commmand() << endl;

    client.server_close();




}


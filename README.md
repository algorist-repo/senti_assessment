# README #
## TCP Server and Client Stream ##
 * ServerClass (ServerClass.cpp)
 * Client Class (ClientClass.cpp)
## Compile ##
```
g++ ServerClass.cpp -o server
g++ ClientClass.cpp -o client
```
## Run ##
In compiled binaries directory createa new directory for sounds:
```
mkdir sound
```
Open two terminals
Run server first terminal:
```
./server 
```
Run Client in second terminal:
```
./client 
```
## Token Authentication ##
### Compile ###
```
g++ TokenizeAuth.cpp -o TokenizeAuth -std=c++11
```
### Run ###
```
./TokenizeAuth
```



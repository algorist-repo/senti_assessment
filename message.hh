#include <iostream>
#include <fstream>
#include <stdio.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


#define PKT_SIZE  256
#define MAX_CLIENT_QUE 3
#define MAGIC 0x1234
#define FILE_NAME 32
#define SERVER_PORT 8888
#define SND_DIR "sound"
#define FILE_SIZE 1024

typedef struct {
    int msg_type;
    int msg_len;
}msg_hdr_t;


typedef struct  {
    msg_hdr_t header;
    char filename[FILE_NAME];
}s_transfer_t;

typedef struct  {
    msg_hdr_t header;
}e_transfer_t;

typedef struct  {
    msg_hdr_t header;
}ack_t;

typedef struct  {
    msg_hdr_t header;     
}nack_t;

typedef struct  {
   msg_hdr_t header;
   unsigned char data[PKT_SIZE];    
}packet_t;

using namespace std;

enum msgtype {
    START_TRANS = 1,
    STREAM_DATA = 2,
    END_TRANS = 3,
    ACK = 4,
    NACK = 5    
};
